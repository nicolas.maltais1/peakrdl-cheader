{% if ds.std.static_assert %}
{%- for node in top_nodes %}
{%- if node.is_array %}
static_assert(sizeof({{ds.type_name_transform(node.type_name)}}) == {{"%#x" % node.array_stride}}, "Packing error");
{%- else %}
static_assert(sizeof({{ds.type_name_transform(node.type_name)}}) == {{"%#x" % node.size}}, "Packing error");
{%- endif %}
{%- endfor %}
{%- endif %}

#pragma pack(pop)

#endif /* {{header_guard_def}} */

// NOLINTEND
