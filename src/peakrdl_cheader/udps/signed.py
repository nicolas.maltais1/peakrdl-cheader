from typing import Any

from systemrdl import Field
from systemrdl.node import Node, FieldNode
from systemrdl.udp import UDPDefinition


class Signed(UDPDefinition):
    name = "signed"
    valid_components = {Field}
    valid_type = bool

    def validate(self, node: 'Node', value: Any) -> None:
        assert isinstance(node, FieldNode)

    def get_unassigned_default(self, node: 'Node') -> Any:
        return False
