from .field_reg_array import FieldRegArray
from .signed import Signed

ALL_UDPS = [
    FieldRegArray,
    Signed,
]
