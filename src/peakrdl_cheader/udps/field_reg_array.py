from typing import Any

from systemrdl import Reg
from systemrdl.node import Node, RegNode
from systemrdl.udp import UDPDefinition


class FieldRegArray(UDPDefinition):
    name = "field_reg_array"
    valid_components = {Reg}
    valid_type = bool

    def validate(self, node: 'Node', value: Any) -> None:
        assert isinstance(node, RegNode)

    def get_unassigned_default(self, node: 'Node') -> Any:
        return False
