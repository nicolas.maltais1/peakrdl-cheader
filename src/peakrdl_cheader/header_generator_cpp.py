import copy
import math
import os
import re
from typing import TextIO, Optional, List, Sequence

from systemrdl.node import AddrmapNode, AddressableNode, RegNode, FieldNode, Node, MemNode, RootNode
from systemrdl.walker import RDLListener, RDLWalker, WalkerAction

from . import utils
from .design_state import DesignState
from .identifier_filter import kw_filter as kwf

FRA_REGEX = re.compile(r"^(.*)_(\d+)$")


class HeaderGeneratorCpp(RDLListener):
    def __init__(self, ds: DesignState) -> None:
        self.ds = ds

        self.indent_level = 0

        self.enums_location = {}
        self.enums_location_node = {}
        self.scope_defs = []
        self.skip_node = False

        self.root_node: AddrmapNode
        self.root_node = None

        self.f: TextIO
        self.f = None  # type: ignore

        assert self.ds.std.is_cpp
        assert not self.ds.generate_defines, "Defines not supported in the C++ header"
        assert not self.ds.instantiate, "Instantiate option not supported in the C++ header"
        assert not self.ds.overlapping_fields, "Overlapping fields not supported in the C++ header"
        assert not self.ds.overlapping_reg_pairs, "Overlapping registers not supported in the C++ header"

    def run(self, path: str, top_nodes: List[AddrmapNode]) -> None:
        with open(path, "w", encoding='utf-8') as f:
            self.f = f

            context = {
                "ds": self.ds,
                "header_guard_def": re.sub(r"[^\w]", "_", os.path.basename(path)).upper(),
                "top_nodes": top_nodes,
            }

            # Stream header via jinja
            template = self.ds.jj_env.get_template("header_cpp.h")
            template.stream(context).dump(f)
            f.write("\n")

            # Generate definitions
            for node in top_nodes:
                self.root_node = node
                self.scope_defs = [set()]
                self.find_enum_locations()
                RDLWalker().walk(node, self)

            # Stream footer via jinja
            template = self.ds.jj_env.get_template("footer_cpp.h")
            template.stream(context).dump(f)

            # Ensure newline before EOF
            f.write("\n")

    def find_enum_locations(self) -> None:
        # The compiler doesn't provide a way to know where the enums were defined originally in the RDL hierarchy.
        # Instead, we find all uses for an enum and use their common parent as the definition location.
        enum_uses = {}

        class Listener(RDLListener):
            def enter_Field(listener, node: FieldNode) -> Optional[WalkerAction]:
                encode = node.get_property("encode")
                if encode is not None:
                    if encode not in enum_uses:
                        enum_uses[encode] = []
                    enum_uses[encode].append(node.parent)
                return WalkerAction.Continue

        RDLWalker().walk(self.root_node, Listener())

        # Find common parent component and build a list of all the enums it contains.
        self.enums_location = {}
        self.enums_location_rev = {}
        for enum, uses in enum_uses.items():
            paths = []
            for use in uses:
                path = []
                while not isinstance(use.parent, RootNode):
                    path.append(use)
                    use = use.parent
                paths.append(path[::-1])

            last_common = None
            for parents in zip(*paths):
                if not all(p is parents[0] for p in parents):
                    break
                last_common = parents[0]

            if last_common.inst not in self.enums_location:
                self.enums_location[last_common.inst] = []
            self.enums_location[last_common.inst].append(enum)

            self.enums_location_rev[enum] = last_common

    def push_indent(self) -> None:
        self.indent_level += 1

    def pop_indent(self) -> None:
        self.indent_level -= 1

    def write(self, s: str) -> None:
        if self.indent_level:
            self.f.write("    " * self.indent_level)
        self.f.write(s)

    def get_node_prefix(self, node: AddressableNode) -> str:
        return utils.get_node_prefix(self.ds, self.root_node, node)

    def get_friendly_name(self, node: Node) -> str:
        return utils.get_friendly_name(self.ds, self.root_node, node)

    def get_type_name(self, node: Node) -> str:
        type_name = kwf(self.ds.type_name_transform(node.type_name))
        if node.type_name == node.parent.type_name and node.type_name != node.parent.parent.type_name:
            # Nested structs cannot have the exact same name as parent
            type_name += "_"
        return type_name

    def get_field_type(self, field: FieldNode) -> str:
        encode = field.get_property("encode")
        if not self.ds.generate_enums or encode is None:
            if field.width == 1:
                return "bool"
            else:
                width = max(8, utils.roundup_pow2(field.width))
                signed = field.get_property("is_signed")
                return f"{'' if signed else 'u'}int{width}_t"
        else:
            return kwf(self.ds.enum_name_transform(encode.type_name))

    def write_bitfields(self, regwidth: int, fields: List[FieldNode]) -> None:
        if not fields:
            return

        self.write("struct {\n")
        self.push_indent()

        def write_pad(width: int) -> None:
            self.write(f"uint{max(8, utils.roundup_pow2(width))}_t : {width:d};\n")

        if self.ds.bitfield_order_ltoh:
            # Bits are packed in struct LSb --> MSb
            current_offset = 0
            for field in fields:
                if field.low > current_offset:
                    write_pad(field.low - current_offset)
                    current_offset = field.low
                self.write(f"{self.get_field_type(field)} {kwf(self.ds.field_name_transform(field.inst_name))} "
                           f": {field.width:d};\n")
                current_offset += field.width

            if current_offset < regwidth:
                write_pad(regwidth - current_offset)
        else:
            # Bits are packed in struct MSb --> LSb
            current_offset = regwidth - 1
            for field in reversed(fields):
                if field.high < current_offset:
                    write_pad(field.low - current_offset)
                    current_offset = field.high
                self.write(f"{self.get_field_type(field)} {kwf(self.ds.field_name_transform(field.inst_name))} "
                           f": {field.width:d};\n")
                current_offset -= field.width

            if current_offset > -1:
                write_pad(current_offset + 1)

        self.pop_indent()
        self.write("} s;\n")

    def enter_Reg(self, node: RegNode) -> Optional[WalkerAction]:
        if self.skip_node:
            return
        assert node.get_property("regwidth") <= 64, "Register width >64 not supported in C++ header"

        self.write(f"// {self.get_friendly_name(node)}\n")

        # No need to traverse fields
        return WalkerAction.SkipDescendants

    def exit_Reg(self, node: RegNode) -> None:
        if self.skip_node:
            return
        fields = list(node.fields())

        base_regwidth = node.get_property('regwidth')
        if node.get_property("field_reg_array"):
            # Only keep fields with a "0" suffix and remove the suffix.
            # The struct will be defined for the field group only and with the appropriate width.
            new_fields = []
            group_started = False
            regwidth = None
            for i, field in enumerate(fields):
                new_name = FRA_REGEX.sub(r"\1", field.inst_name)
                if field.inst_name.endswith("_0"):
                    assert group_started or i == 0, "Field groups in field_reg_array register cannot be interleaved"
                    group_started = True
                    field.inst = copy.deepcopy(field.inst)
                    field.inst.inst_name = new_name
                    field.inst.type_name = field.inst.inst_name
                    new_fields.append(field)
                else:
                    assert new_name == new_fields[i % len(new_fields)].inst_name, \
                        "All field groups in field_reg_array register must be homegeneous"
                    group_started = False
                    if not regwidth:
                        regwidth = field.low - new_fields[0].low
            fields = new_fields
            assert regwidth % 8 == 0, "Field group width in field_reg_array register must be a multiple of 8"
            node.inst.properties["field_group_count"] = base_regwidth // regwidth  # will be used later on
        else:
            regwidth = base_regwidth

        # Generate a union+struct for the register
        self.write(f"union {self.get_type_name(node)} {{\n")
        self.push_indent()
        self.write_enums(node)

        # Write bitfield struct members
        self.write_bitfields(regwidth, fields)

        # Write word member
        self.write_struct_member("reg", f"uint{regwidth}_t")

        self.pop_indent()
        self.write("};\n\n")

    def enter_AddressableComponent(self, node: AddressableNode) -> Optional[WalkerAction]:
        # skip already defined types within the current hierarchy
        type_name = self.get_type_name(node)
        if type_name in self.scope_defs[-1]:
            self.skip_node = True
            return WalkerAction.SkipDescendants
        self.scope_defs[-1].add(type_name)
        self.scope_defs.append(set())

        if isinstance(node, (RegNode, MemNode)):
            # Registers and Mem handled elsewhere
            return WalkerAction.Continue

        self.write(f"// {self.get_friendly_name(node)}\n")
        self.write(f"struct {type_name} {{\n")
        self.push_indent()
        self.write_enums(node)

        return WalkerAction.Continue

    def exit_AddressableComponent(self, node: AddressableNode) -> None:
        if self.skip_node:
            self.skip_node = False
            return

        self.scope_defs.pop()

        if isinstance(node, (RegNode, MemNode)):
            # Registers and Mem handled elsewhere
            return

        self.write_block(node)
        self.pop_indent()
        self.write("};\n\n")

    def enter_Mem(self, node: MemNode) -> Optional[WalkerAction]:
        self.write(f"// {self.get_friendly_name(node)}\n")
        self.write(f"struct {self.get_type_name(node)} {{\n")
        self.push_indent()
        self.write_enums(node)

        return WalkerAction.Continue

    def exit_Mem(self, node: MemNode) -> None:
        if next(iter(node.registers()), None):
            # memory has virtual registers
            self.write_block(node)
        else:
            # no virtual registers, write out an array of words of memwidth
            width = utils.roundup_pow2(node.get_property("memwidth"))
            assert width <= 64, "Mem width >64 not supported in C++ header"
            self.write_struct_member("mem", f"uint{width}_t", (node.get_property("mementries"),))

        self.pop_indent()
        self.write("};\n\n")

    def write_block(self, node: AddressableNode) -> None:
        current_offset = 0
        for child in node.children():
            if not isinstance(child, AddressableNode):
                continue

            # Insert byte padding before next child
            padding = child.raw_address_offset - current_offset
            if padding:
                self.write_byte_padding(current_offset, padding)
                current_offset += padding

            if isinstance(child, RegNode):
                self.write_reg_struct_member(child)
            else:
                self.write_struct_member(child.inst_name, self.get_type_name(child), child.array_dimensions)

            current_offset += child.total_size

        # Write end padding as needed
        if node.is_array:
            padding = node.array_stride - current_offset
        else:
            padding = node.size - current_offset
        if padding:
            self.write_byte_padding(current_offset, padding)

    def write_byte_padding(self, start_offset: int, size: int) -> None:
        self.write_struct_member(f"RESERVED_{start_offset:x}_{start_offset + size - 1:x}", "uint8_t", (size,))

    def write_reg_struct_member(self, node: RegNode) -> None:
        if node.is_array:
            dim = list(node.array_dimensions)
            if node.get_property("field_reg_array"):
                dim[-1] *= node.get_property("field_group_count")
            array_dim = dim
        else:
            array_dim = []

        self.write_struct_member(node.inst_name, self.get_type_name(node), array_dim)

    def write_struct_member(self, name: str, type_name: str, dimen: Optional[Sequence[int]] = None) -> None:
        if dimen:
            self.write(f"{type_name} {kwf(name)}[{']['.join(map(str, dimen))}];\n")
        else:
            self.write(f"{type_name} {kwf(name)};\n")

    def write_enums(self, node: Node) -> None:
        # Write contained enums if any
        for enum in self.enums_location.get(node.inst, []):
            width = max(8, utils.roundup_pow2(math.ceil(max(math.log2(m.value + 1) for m in enum))))
            name = kwf(self.ds.enum_name_transform(enum.type_name))
            self.write(f"enum class {name} : uint{width}_t {{\n")
            self.push_indent()
            for member in enum:
                self.write(f"{kwf(member.name)} = {member.value},\n")
            self.pop_indent()
            self.write("};\n\n")
