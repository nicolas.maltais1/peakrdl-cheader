import copy
from typing import Type, TextIO, Set, Optional, List, Sequence
import os
import re

from systemrdl.walker import RDLListener, RDLWalker, WalkerAction
from systemrdl.node import AddrmapNode, AddressableNode, RegNode, FieldNode, Node, MemNode

from .design_state import DesignState
from .identifier_filter import kw_filter as kwf
from . import utils

FRA_REGEX = re.compile(r"^(.*)_(\d+)$")

class HeaderGenerator(RDLListener):
    def __init__(self, ds: DesignState) -> None:
        self.ds = ds

        self.defined_namespace: Set[str]
        self.defined_namespace = set()
        self.indent_level = 0

        self.root_node: AddrmapNode
        self.root_node = None

        self.f: TextIO
        self.f = None # type: ignore

    def run(self, path: str, top_nodes: List[AddrmapNode]) -> None:
        with open(path, "w", encoding='utf-8') as f:
            self.f = f

            context = {
                "ds": self.ds,
                "header_guard_def": re.sub(r"[^\w]", "_", os.path.basename(path)).upper(),
                "top_nodes": top_nodes,
                "get_struct_name": utils.get_struct_name,
            }

            # Stream header via jinja
            template = self.ds.jj_env.get_template("header.h")
            template.stream(context).dump(f)
            f.write("\n")

            # Write enums
            if self.ds.generate_enums:
                self.write_enums(top_nodes)

            # Generate definitions
            for node in top_nodes:
                self.root_node = node
                RDLWalker().walk(node, self)

            # Write direct instance definitions
            if self.ds.instantiate:
                f.write("\n// Instances\n")
                for node in top_nodes:
                    addr = node.raw_absolute_address + self.ds.inst_offset
                    type_name = utils.get_struct_name(self.ds, node, node)
                    if node.is_array:
                        if len(node.array_dimensions) > 1:
                            node.env.msg.fatal(
                                f"C header generator does not support instance defines for multi-dimensional arrays: {node.inst_name}{node.array_dimensions}",
                                node.inst.inst_src_ref
                            )
                        f.write(f"#define {node.inst_name} ((volatile {type_name} *){addr:#x}UL)\n")
                    else:
                        f.write(f"#define {node.inst_name} (*(volatile {type_name} *){addr:#x}UL)\n")

            # Stream footer via jinja
            template = self.ds.jj_env.get_template("footer.h")
            template.stream(context).dump(f)

            # Ensure newline before EOF
            f.write("\n")

    def push_indent(self) -> None:
        self.indent_level += 1

    def pop_indent(self) -> None:
        self.indent_level -= 1

    def write(self, s: str) -> None:
        if self.indent_level:
            self.f.write("    " * self.indent_level)
        self.f.write(s)


    def get_node_prefix(self, node: AddressableNode) -> str:
        return utils.get_node_prefix(self.ds, self.root_node, node)

    def get_struct_name(self, node: AddressableNode) -> str:
        return utils.get_struct_name(self.ds, self.root_node, node)

    def get_friendly_name(self, node: Node) -> str:
        return utils.get_friendly_name(self.ds, self.root_node, node)

    def write_bitfields(self, grp_name: str, regwidth: int, fields: List[FieldNode]) -> None:
        if not fields:
            return

        if regwidth > 64:
            # TODO: add support for this
            self.root_node.env.msg.fatal(
                "C header bit-fields for registers wider than 64-bits is not supported yet",
                fields[0].parent.inst.inst_src_ref
            )

        self.write("struct {\n")
        self.push_indent()

        def get_field_type(field: FieldNode) -> str:
            encode = field.get_property("encode")
            if not self.ds.generate_enums or encode is None:
                if field.width == 1:
                    return "bool"
                else:
                    width = max(8, utils.roundup_pow2(field.width))
                    signed = field.get_property("is_signed")
                    return f"{'' if signed else 'u'}int{width}_t"
            else:
                return f"{self.get_enum_prefix(encode)}_e"

        def write_pad(width: int) -> None:
            self.write(f"uint{max(8, utils.roundup_pow2(width))}_t : {width:d};\n")

        if self.ds.bitfield_order_ltoh:
            # Bits are packed in struct LSb --> MSb
            current_offset = 0
            for field in fields:
                if field.low > current_offset:
                    write_pad(field.low - current_offset)
                    current_offset = field.low
                self.write(f"{get_field_type(field)} {kwf(self.ds.field_name_transform(field.inst_name))} "
                           f": {field.width:d};\n")
                current_offset += field.width

            if current_offset < regwidth:
                write_pad(regwidth - current_offset)
        else:
            # Bits are packed in struct MSb --> LSb
            current_offset = regwidth - 1
            for field in reversed(fields):
                if field.high < current_offset:
                    write_pad(field.low - current_offset)
                    current_offset = field.high
                self.write(f"{get_field_type(field)} {kwf(self.ds.field_name_transform(field.inst_name))} "
                           f": {field.width:d};\n")
                current_offset -= field.width

            if current_offset > -1:
                write_pad(current_offset + 1)

        self.pop_indent()
        self.write(f"}} {grp_name};\n")

    def enter_Reg(self, node: RegNode) -> Optional[WalkerAction]:
        prefix = self.get_node_prefix(node).upper()

        if prefix in self.defined_namespace:
            return WalkerAction.SkipDescendants
        self.defined_namespace.add(prefix)

        self.write(f"\n// {self.get_friendly_name(node)}\n")

        if self.ds.generate_defines:
            for field in node.fields():
                field_prefix = prefix + "_" + field.inst_name.upper()

                bm = ((1 << field.width) - 1) << field.low
                self.write(f"#define {field_prefix}_bm {bm:#x}\n")
                self.write(f"#define {field_prefix}_bp {field.low:d}\n")
                self.write(f"#define {field_prefix}_bw {field.width:d}\n")

                reset = field.get_property('reset')
                if isinstance(reset, int):
                    self.write(f"#define {field_prefix}_reset {reset:#x}\n")

        # No need to traverse fields
        return WalkerAction.SkipDescendants


    def exit_Reg(self, node: RegNode) -> None:
        if not self.ds.generate_bitfields:
            return

        union_name = self.get_struct_name(node)
        if union_name in self.defined_namespace:
            # Already defined. Skip
            return
        self.defined_namespace.add(union_name)

        # Sort fields into their respective categories
        overlapping_fields = self.ds.overlapping_fields.get(node.get_path(), [])
        fr_fields = []
        fw_fields = []
        f_fields = []
        for field in node.fields():
            if field.inst_name in overlapping_fields:
                # Is an overlapping field.
                # Guaranteed to be either read-only or write-only
                if field.is_sw_readable:
                    fr_fields.append(field)
                else:
                    fw_fields.append(field)
            else:
                f_fields.append(field)

        base_regwidth = node.get_property('regwidth')
        if node.get_property("field_reg_array"):
            # Only keep fields with a "0" suffix and remove the suffix. Assume no overlapping fields.
            # The struct will be defined for the field group only and with the appropriate width.
            assert len(fw_fields) == 0 and len(fr_fields) == 0
            new_fields = []
            group_started = False
            regwidth = None
            for i, field in enumerate(f_fields):
                new_name = FRA_REGEX.sub(r"\1", field.inst_name)
                if field.inst_name.endswith("_0"):
                    assert group_started or i == 0, "Field groups in field_reg_array register cannot be interleaved"
                    group_started = True
                    field.inst = copy.deepcopy(field.inst)
                    field.inst.inst_name = new_name
                    field.inst.type_name = field.inst.inst_name
                    new_fields.append(field)
                else:
                    assert new_name == new_fields[i % len(new_fields)].inst_name, \
                        "All field groups in field_reg_array register must be homegeneous"
                    group_started = False
                    if not regwidth:
                        regwidth = field.low - new_fields[0].low
            f_fields = new_fields
            assert regwidth % 8 == 0, "Field group width in field_reg_array register must be a multiple of 8"
            node.inst.properties["field_group_count"] = base_regwidth // regwidth  # will be used later on
        else:
            regwidth = base_regwidth

        # Generate a union+struct for the register
        self.write("typedef union {\n")
        self.push_indent()

        # Write bitfield struct members
        self.write_bitfields("s", regwidth, f_fields)
        self.write_bitfields("sr", regwidth, fr_fields)
        self.write_bitfields("sw", regwidth, fw_fields)

        # Write word member
        if regwidth > 64:
            # Cannot use stdint types
            # Build the wide register as an array of smaller stdint types
            n_subwords = regwidth // self.ds.wide_reg_subword_size
            self.write_struct_member("reg", f"uint{self.ds.wide_reg_subword_size}_t", (n_subwords,))
        else:
            self.write_struct_member("reg", f"uint{regwidth}_t")

        self.pop_indent()
        self.write(f"}} {union_name};\n")


    def exit_AddressableComponent(self, node: AddressableNode) -> None:
        if isinstance(node, (RegNode, MemNode)):
            # Registers and Mem handled elsewhere
            return

        self.write_block(node)

    def exit_Mem(self, node: MemNode) -> None:
        for _ in node.registers():
            has_vregs = True
            break
        else:
            has_vregs = False


        if has_vregs:
            # Contains virtual registers.
            # Write out as if it is a regular block
            self.write_block(node)
            return

        # otherwise, write out an array of words of memwidth
        struct_name = self.get_struct_name(node)
        if struct_name in self.defined_namespace:
            # Already defined. Skip
            return
        self.defined_namespace.add(struct_name)

        self.write(f"\n// {self.get_friendly_name(node)}\n")

        self.write("typedef struct {\n")
        self.push_indent()

        width = utils.roundup_pow2(node.get_property("memwidth"))
        if width > 64:
            n_subwords = width // self.ds.wide_reg_subword_size
            self.write_struct_member("mem", f"uint{self.ds.wide_reg_subword_size}_t", (n_subwords,))
        else:
            self.write_struct_member("mem", f"uint{width}_t", (node.get_property("mementries"),))

        self.pop_indent()
        self.write(f"}} {struct_name};\n")

    def write_block(self, node: AddressableNode) -> None:
        struct_name = self.get_struct_name(node)
        if struct_name in self.defined_namespace:
            # Already defined. Skip
            return
        self.defined_namespace.add(struct_name)

        self.write(f"\n// {self.get_friendly_name(node)}\n")

        self.write("typedef struct {\n")
        self.push_indent()

        current_offset = 0
        skipme = set()
        for child in node.children():
            if not isinstance(child, AddressableNode):
                continue

            # Skip any child regs that were already emitted due to overlap union
            if child.inst_name in skipme:
                continue

            # Insert byte padding before next child
            padding = child.raw_address_offset - current_offset
            if padding:
                self.write_byte_padding(current_offset, padding)
                current_offset += padding

            if isinstance(child, RegNode):
                # Check if register is overlapping first
                partner_reg_name = self.ds.overlapping_reg_pairs.get(child.get_path(), None)
                if partner_reg_name:
                    # Two registers occupy the same space
                    self.write("union {\n")
                    self.push_indent()
                    self.write_reg_struct_member(child)
                    self.write_reg_struct_member(node.get_child_by_name(partner_reg_name))
                    self.pop_indent()
                    if self.ds.std.anon_unions:
                        self.write("};\n")
                    else:
                        self.write(f"}} {child.inst_name}_{partner_reg_name};\n")
                    skipme.add(partner_reg_name)
                else:
                    self.write_reg_struct_member(child)
            else:
                child_struct_name = self.get_struct_name(child)
                self.write_struct_member(child.inst_name, child_struct_name, child.array_dimensions)

            current_offset += child.total_size

        # Write end padding as needed
        if node.is_array:
            padding = node.array_stride - current_offset
        else:
            padding = node.size - current_offset
        if padding:
            self.write_byte_padding(current_offset, padding)

        self.pop_indent()
        self.write(f"}} {struct_name};\n")


    def write_byte_padding(self, start_offset: int, size: int) -> None:
        self.write_struct_member(f"RESERVED_{start_offset:x}_{start_offset + size - 1:x}", "uint8_t", (size,))


    def write_reg_struct_member(self, node: RegNode) -> None:
        if node.is_array:
            dim = list(node.array_dimensions)
            if node.get_property("field_reg_array"):
                dim[-1] *= node.get_property("field_group_count")
            array_dim = dim
        else:
            array_dim = []

        if self.ds.generate_bitfields:
            struct_name = self.get_struct_name(node)
            self.write_struct_member(node.inst_name, struct_name, array_dim)
        else:
            regwidth = node.get_property('regwidth')
            if regwidth > 64:
                # Cannot use stdint types
                # Build the wide register as an array of smaller stdint types
                n_subwords = regwidth // self.ds.wide_reg_subword_size
                type_name = f"uint{self.ds.wide_reg_subword_size}_t"
                self.write_struct_member(node.inst_name, type_name, array_dim + [n_subwords])
            else:
                self.write_struct_member(node.inst_name, f"uint{regwidth}_t", array_dim)


    def write_struct_member(self, name: str, type_name: str, dimen: Optional[Sequence[int]] = None) -> None:
        if dimen:
            self.write(f"{type_name} {kwf(name)}[{']['.join(str(d) for d in dimen)}];\n")
        else:
            self.write(f"{type_name} {kwf(name)};\n")


    @staticmethod
    def get_enum_prefix(user_enum: Type['UserEnum']) -> str:
        scope = user_enum.get_scope_path("_")
        if scope:
            return f"{scope}_{user_enum.type_name}"
        else:
            return user_enum.type_name

    def write_enum(self, user_enum: Type['UserEnum']) -> None:
        prefix = self.get_enum_prefix(user_enum)
        lines = []

        for enum_member in user_enum:
            lines.append(f"    {prefix}_{enum_member.name} = {enum_member.value}")
        self.write("typedef enum {\n"
                   + ",\n".join(lines)
                   + f"\n}} {prefix}_e;\n")

    def write_enums(self, top_nodes: List[AddrmapNode]) -> None:
        user_enums = []

        class Listener(RDLListener):
            def enter_Field(listener, node: FieldNode) -> Optional[WalkerAction]:
                encode = node.get_property("encode")
                if encode is not None and encode not in user_enums:
                    user_enums.append(encode)
                return None

        for node in top_nodes:
            self.root_node = node
            RDLWalker().walk(node, Listener())

        for user_enum in user_enums:
            self.write("\n")
            self.write_enum(user_enum)
