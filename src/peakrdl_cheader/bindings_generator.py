import copy
import re
from pathlib import Path
from typing import Set, Optional, List, Sequence

from systemrdl.node import AddrmapNode, AddressableNode, RegNode, Node, MemNode, RootNode
from systemrdl.walker import RDLWalker, WalkerAction

from . import utils
from .design_state import DesignState
from .header_generator_cpp import HeaderGeneratorCpp
from .identifier_filter import kw_filter as kwf, kw_filter_python as kwfp

FRA_REGEX = re.compile(r"^(.*)_(\d+)$")


class BindingsGenerator(HeaderGeneratorCpp):
    def __init__(self, ds: DesignState) -> None:
        super().__init__(ds)

        self.defined_namespace: Set[str]
        self.defined_namespace = set()

    def run(self, path: str, top_nodes: List[AddrmapNode]) -> None:
        # Note: we assume HeaderGeneratorCpp was run before and thus that the "field_group_count" property are set.
        path = Path(path)
        context = {
            "name": path.stem,
        }

        with open(path.parent / f"{path.stem}_bindings.h", "w", encoding='utf-8') as f:
            template = self.ds.jj_env.get_template("bindings.h")
            template.stream(context).dump(f)
            f.write("\n")

        with open(path.parent / f"{path.stem}_bindings.cpp", "w", encoding='utf-8') as f:
            self.f = f

            template = self.ds.jj_env.get_template("header_bindings.cpp")
            template.stream(context).dump(f)
            f.write("\n")
            self.push_indent()

            # Generate definitions
            for node in top_nodes:
                self.root_node = node
                self.find_enum_locations()
                RDLWalker().walk(node, self)

            self.pop_indent()
            template = self.ds.jj_env.get_template("footer_bindings.cpp")
            template.stream(context).dump(f)
            f.write("\n")

    def get_struct_name(self, node: AddressableNode) -> str:
        return utils.get_struct_name(self.ds, self.root_node, node, suffix="")

    def get_qualified_name(self, node: Node) -> str:
        s = ""
        while not isinstance(node, RootNode):
            s = f"{self.get_type_name(node)}::{s}"
            node = node.parent
        return s[:-2]

    def get_type_name_p(self, node: Node) -> str:
        return kwfp(self.ds.type_name_transform(node.type_name))

    def enter_Reg(self, node: RegNode) -> Optional[WalkerAction]:
        struct_name = self.get_struct_name(node)
        if struct_name in self.defined_namespace:
            # Already defined. Skip
            return WalkerAction.SkipDescendants
        self.defined_namespace.add(struct_name)

        type_name = self.get_type_name_p(node)
        qualified_name = self.get_qualified_name(node)

        parent = node.parent
        parent_struct_name = "mod" if isinstance(parent, RootNode) else self.get_struct_name(parent)
        self.write(f"// {self.get_friendly_name(node)}\n")
        self.write(f'auto {struct_name} = py::class_<{qualified_name}>({parent_struct_name}, "{type_name}")\n')
        self.push_indent()
        self.write_member_binding("s", qualified_name)
        self.write_member_binding("reg", qualified_name, readonly=False, last=True)
        self.pop_indent()

        self.write(f'py::class_<{qualified_name}::S>({struct_name}, "{type_name}_s")\n')
        self.push_indent()
        fields = list(node.fields())
        if node.get_property("field_reg_array"):
            # Only keep fields with a "0" suffix and remove the suffix.
            # The struct will be defined for the field group only and with the appropriate width.
            new_fields = []
            for i, field in enumerate(fields):
                new_name = FRA_REGEX.sub(r"\1", field.inst_name)
                if field.inst_name.endswith("_0"):
                    field.inst = copy.deepcopy(field.inst)
                    field.inst.inst_name = new_name
                    field.inst.type_name = field.inst.inst_name
                    new_fields.append(field)
            fields = new_fields

        for i, field in enumerate(fields):
            tname = self.ds.field_name_transform(field.inst_name)
            name = kwf(tname)
            name_p = kwfp(tname)

            readonly = not field.is_sw_writable
            suffix = ";\n" if i == len(fields) - 1 else "\n"
            if readonly:
                self.write(f'.def_property_readonly("{name_p}", '
                           f'[]({qualified_name}::S& s) {{ return s.{name}; }}){suffix}')
            else:
                field_type = self.get_field_type(field)
                encode = field.get_property("encode")
                if self.ds.generate_enums and encode is not None:
                    field_type = f"{self.get_qualified_name(self.enums_location_rev[encode])}::{field_type}"
                self.write(f'.def_property("{name_p}", '
                           f'[]({qualified_name}::S& s) {{ return s.{name}; }},\n')
                self.write(f'              []({qualified_name}::S& s, {field_type} v) {{ s.{name} = v; }}){suffix}')
        self.pop_indent()
        self.write("\n")

        self.write_enums(node)

        return WalkerAction.SkipDescendants

    def exit_Reg(self, node: RegNode) -> None:
        pass

    def enter_AddressableComponent(self, node: AddressableNode) -> Optional[WalkerAction]:
        if isinstance(node, RegNode):
            # Reg is handled elsewhere
            return WalkerAction.Continue

        struct_name = self.get_struct_name(node)
        if struct_name in self.defined_namespace:
            # Already defined. Skip
            return WalkerAction.SkipDescendants
        self.defined_namespace.add(struct_name)

        type_name = self.get_type_name_p(node)
        qualified_name = self.get_qualified_name(node)

        parent = node.parent
        parent_struct_name = "mod" if isinstance(parent, RootNode) else self.get_struct_name(parent)
        self.write(f"// {self.get_friendly_name(node)}\n")
        self.write(f'auto {struct_name} = py::class_<{qualified_name}>({parent_struct_name}, "{type_name}")\n')
        self.push_indent()
        if not isinstance(node, MemNode) or next(node.registers(), None):
            # addrmap, regfile, or memory with virtual registers
            self.write_block(node)
        else:
            # memory with no virtual registers
            width = utils.roundup_pow2(node.get_property("memwidth"))
            self.write_member_binding("mem", qualified_name, (node.get_property("mementries"),), last=True)
        self.pop_indent()
        self.write("\n")

        self.write_enums(node)

        return WalkerAction.Continue

    def exit_AddressableComponent(self, node: AddressableNode) -> None:
        pass

    def enter_Mem(self, node: MemNode) -> Optional[WalkerAction]:
        return WalkerAction.Continue

    def exit_Mem(self, node: MemNode) -> None:
        pass

    def write_block(self, node: AddressableNode) -> None:
        children = list(node.children())
        qualified_name = self.get_qualified_name(node)
        for i, child in enumerate(children):
            if not isinstance(child, AddressableNode):
                continue

            last = i == len(children) - 1
            if isinstance(child, RegNode):
                self.write_reg_member_binding(child, qualified_name, last)
            else:
                self.write_member_binding(child.inst_name, qualified_name, child.array_dimensions, last=last)

    def write_reg_member_binding(self, node: RegNode, parent_qualified_name: str, last: bool) -> None:
        if node.is_array:
            dim = list(node.array_dimensions)
            if node.get_property("field_reg_array"):
                dim[-1] *= node.get_property("field_group_count")
            array_dim = dim
        else:
            array_dim = []

        self.write_member_binding(node.inst_name, parent_qualified_name, array_dim, last=last)

    def write_member_binding(self, tname: str, parent_qualified_name: str,
                             dimen: Optional[Sequence[int]] = None,
                             readonly: bool = True, last: bool = False) -> None:
        name = kwf(tname)
        name_p = kwfp(tname)
        suffix = ";\n" if last else "\n"
        if dimen:
            assert readonly
            self.write(f'.def_property_readonly("{name_p}", [](py::object& obj) {{\n')
            self.push_indent()
            self.write(f'auto& o = obj.cast<{parent_qualified_name}&>();\n')
            self.write(f'return py::array{{std::vector<size_t>{{{", ".join(map(str, dimen))}}}, o.{name}, obj}};\n')
            self.pop_indent()
            self.write(f"}}){suffix}")
        else:
            func = "def_readonly" if readonly else "def_readwrite"
            self.write(f'.{func}("{name_p}", &{parent_qualified_name}::{name}){suffix}')

    def write_enums(self, node: AddressableNode) -> None:
        # Write contained enums if any
        qualified_prefix = self.get_qualified_name(node)
        struct_name = self.get_struct_name(node)

        for enum in self.enums_location.get(node.inst, []):
            tname = self.ds.enum_name_transform(enum.type_name)
            name = kwf(tname)
            name_p = kwfp(tname)
            enum_type = f"{qualified_prefix}::{name}"

            self.write(f'py::enum_<{enum_type}>({struct_name}, "{name_p}")\n')
            self.push_indent()

            members = list(enum)
            for i, member in enumerate(members):
                suffix = ";\n" if i == len(members) - 1 else "\n"
                self.write(f'.value("{kwfp(member.name)}", {enum_type}::{kwf(member.name)}){suffix}')

            self.pop_indent()
            self.write("\n")
